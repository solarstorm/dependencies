#!/usr/bin/env bash

set -e
set -x

if [[ $DRONE_COMMIT_MESSAGE == *"[BUILD]"* ]]; then
    git clone $DRONE_GIT_HTTP_URL .
	git checkout $DRONE_COMMIT

	echo "===================================================================================="
	echo "===================================================================================="
	echo "===================================================================================="
		
else
	echo "DONT found [BUILD] in commit message, skipping cloning!"
    exit 1
fi

